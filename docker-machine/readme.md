##### Exercise 1


```bash
docker-machine create -d virtualbox small0
docker-machine create -d virtualbox --virtualbox-memory "4096" --virtualbox-cpu-count "2" --virtualbox-disk-size "40000" small1
docker-machine create -d virtualbox --virtualbox-memory "4096" --virtualbox-cpu-count "2" --virtualbox-disk-size "40000" small2
```

##### Exercise 2

```bash
docker-machine ls
docker-machine ls --format='{{.Name}} - {{.URL}} - {{ .State }}'
docker-machine inspect --format='{{.Name}} - {{.Driver.IPAddress}}' small1

```

##### Exercise 3

```bash
eval "$(docker-machine env small2)"

docker-compose -f ./exercise_3/my-containers.yml up

docker-compose -f ./exercise_3/my-containers.yml ps

```

##### Exercise 4

```bash
cd exercise_4
docker-compose up
docker-machine inspect --format='{{.Driver.IPAddress}}' small2
curl $(docker-machine inspect --format='{{.Driver.IPAddress}}' small2)
```

##### Exercise 5

```bash
docker-compose stop ubuntu
docker-compose rm ubuntu
docker-compose scale nginx=10
docker-compose run nginx ifconfig
# (run suprascrie CMD containerului si execută cu comanda data).
docker-compose exec --privileged --index=9 nginx bash
docker-compose rm
```

##### Exercise 6

```bash
docker-machine create -d virtualbox small3
eval "$(docker-machine env small3)"
docker build -f "Dockerfile" .
docker build -t "workshop/nginx" .
```

##### Exercise 7

```bash
docker build -f "Dockerfile" .
docker build -t "workshop/nginx" .
```

##### Exercise 8

```bash
sudo docker run -p 8080:80 -d workshop/nginx nginx -g 'daemon off;'
sudo docker-compose up
```

##### Exercise 8

```bash
sudo docker build -t "workshop/nginx" .
sudo docker run -p 8080:80 -d workshop/nginx  
sudo docker-compose up -d
```