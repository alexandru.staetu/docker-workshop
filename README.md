#### Installation on Ubuntu via apt-get
##### Installation of Docker.
```bash
sudo apt-get install -y docker.io
```

Testing
```bash
docker --version
systemctl status docker
```
##### Installation of newer version of Docker

###### Windows 7 
[Get docker ce for windows 7](https://docs.docker.com/toolbox/toolbox_install_windows/)

###### Centos
[Get docker ce for centos](https://docs.docker.com/engine/installation/linux/docker-ce/centos/)

###### Ubuntu
[Get Docker CE for Ubuntu](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/)

```bash
sudo apt-get update
```

```bash
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get install docker-ce
```

Testing

```bash
docker --version
systemctl status docker
```

###### Windows

[Docker Community Edition for Windows](https://store.docker.com/editions/community/docker-ce-desktop-windows)

```bash
docker --version
```

#### Dockerizing applications

##### The hello world example

<details>
    <summary>Click to expand</summary>
    <p> Docker will ensure that the image is present locally, downloading it if needed and run a container from it. </p>
    <p> It will run the starting command( CMD is a hello-world executable ) and stop the container when the process is done. </p>
    <p> We list all containers ( includes exited container ) and limit the result to 1 row so that we can see it's state and start command </p>
</details>

```bash
sudo docker run hello-world

sudo docker ps -a -n 1
```

##### Interactive container

<details>
    <summary>Click to expand</summary>
    <p> We are running a container that can receive data from stdin ( -i ) and had assigned a virtual terminal. </p>
    <p> The starting process for this container is a linux shell and as long as it is running, the container will be up. </p>
    <p> When the user leaves the shell, it's process exits/finishes and the container is stopped </p>
</details>

```bash
docker run -it debian bash
```

##### Demonizing programs

<details>
    <summary>Click to expand</summary>
    <p> We will create our own image that runs a process for a short time and exits </p>
    <p> Docker images are build from layers, each acting as a cache in the build process. </p>
    <p> Each Dockerfile instruction will start new container ( da653cee0545 ), run the provided instruction and create the layer ( untagged/unnamed image ). </p>
    <p> When the build process is finished, an image is created ( ed874adf813a ) </p>
</details>

```bash
sudo docker build ./daemonize/
```
```text
Sending build context to Docker daemon 2.048 kB
Step 1/2 : FROM debian
 ---> da653cee0545
Step 2/2 : CMD sleep 10
 ---> Using cache
 ---> ed874adf813a
Successfully built ed874adf813a
```

```bash
sudo docker run ed874adf813a
```

#### Container usage
##### Running a webapp in a container

<details>
    <summary>Click to expand</summary>
    <p> We will run 3 http examples, starting from a mock application, running an apache instance and finish with a php application </p>
</details>

1. Mock container

<details>
    <summary>Click to expand</summary>
    <p> We create an image which starts a netcat process. </p>
    <p> netcat -l -s -p 80 </p>
    <p> This process listens (-l) on http port (-p) 80, accepts a connection and exits. </p>
    <p> When the process receives content via network, it will print it to stdout and exit/stop </p>
    <p> We will then get the container's ip, send content to it and show the logs so that we can see any process output </p>
</details>

```bash
sudo docker build ./webapp-mock -t webapp-mock
sudo docker run -d --name webapp-mock-instance webapp-mock 
sudo docker inspect webapp-mock-instance  -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'
```

```text
172.17.0.3
```

```bash
echo "Hello world" | telnet 172.17.0.3 80
```

2. An apache container

<details>
    <summary>Click to expand</summary>
    <p> We run an existing apache image to render html elements in browser </p>
    <p> When mapping ports from containers to the host, docker will route all requests done locally to the port belonging to the container and sent to the apache process </p>
    <p> In order to provide html code to the container instance, a local directory can be mounted in the container, at the /usr/local/apache2/htdocs/ path </p>
</details>

```bash
sudo docker run -d -p 80:80 -v "$PWD"/webapp-html:/usr/local/apache2/htdocs/ httpd:2.4
curl localhost
```

3. An apache with php container

<details>
    <summary>Click to expand</summary>
    <p> We run an apache image that has the php module enabled and can render php code </p>
</details>

```bash
sudo docker run -d -p 80:80 -v "$PWD"/webapp-php:/var/www/html php:7.0-apache
curl localhost
```

##### Investigating a container

<details>
    <summary>Click to expand</summary>
    <p> We can get detailed information on any container </p>
    <p> Ex, mounted containers, networks attached to the container, if it can receive input, image from which was run, path on local filesystem where it's files are located </p>
</details>

```bash
sudo docker run --name debian_inspect debian

sudo docker inspect debian_inspect 
```

##### Port mapping

<details>
    <summary>Click to expand</summary>
    <p> When a container is created, it is automatically added to a default docker network ( default bridge ) which is routed to the outside workd </p>
    <p> The container can access and exit the internet but any network hosts will not be able to connect to it </p>
    <p> The docker api allow to map a host port ( ex: port on the external interface ) to a port opened on the container. </p>
    <p> Every request to the host port will automatically be forwarded via iptables to the container port </p>
</details>

```bash
sudo docker run -p 8080:80 webapp-mock
```

```bash
echo "hello_world" | telnet localhost 8080
```

##### Viewing the logs

<details>
    <summary>Click to expand</summary>
    <p> We will run the image previously created to send data to the container and have the main process output it to stderr. </p>
    <p> By default, containers will save the stdout and stderr of a container. This helps investigating the reason a container failed, for example. </p>
    <p> Logs can be extracted for all containers ( exited, stopping, running ) </p>
</details>

```bash
sudo docker run --name webapp-mock-detached -d -p  8080:80 webapp-mock

echo "Hello logs" | telnet localhost 8080

sudo docker logs webapp-mock-detached
```

##### Looking at processes

<details>
    <summary>Click to expand</summary>
    <p> Here we will run two containers in a detached mode ( -d ) and list the running docker processes. </p>
    <p> By default, docker truncates the id for every container to a short form. The long form is useful when looking for referenced containers when investigating volumes, networks, stacks </p>
    <p> When listing processes, you can see different contaiener properties ( state, command used to start the container, it's tag ) or filter by them </p>
</details>

```bash
sudo docker run -d --name=docker-sleep-3600 alpine sleep 3600
sudo docker run -d --name=docker-sleep-7200 alpine sleep 7200
sudo docker ps --no-trunc -a
sudo docker ps -a --no-trunc --filter "status=running"
```

##### Stopping and restarting

<details>
    <summary>Click to expand</summary>
    <p> When a container exits, it's filesystem is persisted on the host's disk </p>
    <p> We launch a shell in a named container and exit the shell thus stopping the container </p>
    <p> We then make sure the container is stopped and then start the container with an attached (-a) shell ( CMD is preserved ) </p>
</details>

```bash
sudo docker run -it --name=stopped-container debian bash
> touch /hello-stopped-container
> exit
sudo docker inspect stopped-container -f '{{.State.Status}}'
sudo docker start -ai stopped-container
> ls /hello-stopped-container
```
##### Removing a container

<details>
    <summary>Click to expand</summary>
    <p> A container is run in detached mode with a long running process keeping it up. </p>
    <p> Durring runtime, a container cannot be removed so we have to stop it </p>
    <p> We ensure that the container is stopped, remove it and ensure that it is no longer present </p>
</details>

```bash
sudo docker run -d --name alpine-sleep alpine sleep 3600

sudo docker rm alpine-sleep

# Command should fail as running containers cannot be removed
sudo docker stop alpine-sleep

sudo docker inspect alpine-sleep  -f '{{.State.Status}}'

sudo docker rm alpine-sleep

# Command should fail as the container is no longer present
sudo docker inspect alpine-sleep 
```
#### Managing images
##### Listing images

<details>
    <summary>Click to expand</summary>
    <p> In this step, we will create an image with different tags. </p>
    <p> Being able to create different versions for the same image allows application versioning ( instantly run apache2.2 or apache2.4 ), green-blue deployments, etc </p>
    <p> If no tag is specified for an image, it is asumed to be the default/latest version </p>
    <p> Listing an image will also show all of it's tags/versions </p>
    <p> We will create an additional image tag ( -t ) (:hello-world:faulty), list the list of tags associated with this image, remove the faulty tag and re-list the image's tag  </p>
</details>

```bash
sudo docker build -t workshop/hello-world workshop/hello-world
sudo docker run workshop/hello-world

sudo docker build -t workshop/hello-world:1 workshop/hello-world-1
sudo docker run workshop/hello-world:1

sudo docker build -t workshop/hello-world:1-beta workshop/hello-world-1-beta
sudo docker run workshop/hello-world:1-beta

sudo docker build -t workshop/hello-world:faulty workshop/hello-world-faulty
sudo docker images workshop/hello-world:faulty

sudo docker images workshop/hello-world

sudo docker rmi workshop/hello-world:faulty
sudo docker images workshop/hello-world:faulty

sudo docker images workshop/hello-world
```
##### Downloading images
```bash
sudo docker pull nginx
sudo docker images nginx
```
##### Finding images
```bash
sudo docker search redis
```
#### Networking of containers
##### Port mapping details

<details>
    <summary>Click to expand</summary>
    <p> By default, docker will create a set of networks that the container can use </p>
    <p> Bridge network acts as network switch, connecting all running containers among them. </p>
    <p> The default docker bridge is routed to the outside via the docker0 host interface and iptable rules </p>
</details>

```bash
sudo docker network ls
sudo docker network inspect bridge
ip addr show dev docker0
sudo iptables -t nat -L -n
```

<details>
    <summary>Click to expand</summary>
    <p> We will check if any running docker is connected to the default bridge network. </p>
    <p> After we run a port-mapped container, we will inspect the default bridge and see if there are containers attached to it ( default operation on container create ) </p>
    <p> We also check the ip address associated to the container to make sure it corresponds to the above output </p>
    <p> We run the iptables command to show how the host port is forwarded to the container port </p>
</details>

```bash
sudo docker network inspect bridge -f '{{.Containers}}'
sudo docker run -d -p 8080:80 --name network-container alpine nc -l -p 80
sudo docker network inspect bridge -f '{{.Containers}}'
sudo docker exec -it network-container ip addr
sudo docker network inspect bridge

sudo iptables -t nat -L -n
```
##### Linking via user defined networks

<details>
    <summary>Click to expand</summary>
    <p> User defined network are a layer of isolation to the for the traffic that a set of containers perform. </p>
    <p> Containers placed on a user defined network cannot access hosts on other user defined networks</p>
    <p> When they are created, user defined networks use bridges as the default docker network. </p>
    <p> User defined networks offer name resolutions among containers placed on the network </p>
    <p> In the example, we create a new network and start two containers on it. We then make sure that the hostnames are visible among containers </p>
</details>

```bash
sudo docker network create workshop-network
sudo docker run -d --network=workshop-network --name networked-container-1 alpine sleep 3600
sudo docker run -d --network=workshop-network --name networked-container-2 alpine sleep 3600
sudo docker inspect networked-container-1 -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'
sudo docker exec -it networked-container-2 cat /etc/hosts
sudo docker exec -it networked-container-2 ping -c 1 networked-container-1
```
#### Data in containers

##### Data volumes

<details>
    <summary>Click to expand</summary>
    <p> Docker volumes are a way to share data between containers. When a volume is created, data stored in it is independed on the containers's lifecicle </p>
    <p> We will create a named volume, create a file in it using a temporary volume and check if the data can be reused from other/new containers </p>
</details>

```bash
sudo docker volume create --name workshop-volume
sudo docker run -v workshop-volume:/mounted-volume debian ls -alts /mounted-volume
sudo docker run -v workshop-volume:/mounted-volume alpine touch /mounted-volume/hello-world
sudo docker run -v workshop-volume:/mounted-volume debian ls -alts /mounted-volume
```
##### Host directories as data volume

<details>
    <summary>Click to expand</summary>
    <p> By using bind mounts, data can be shared directly between the host and the container. ( ex: development environments ) </p>
    <p> Bind mounts require absolute paths </p>
    <p> We will launch a temporary container which we will use to create a file in a mounted directory </p>
</details>

```bash
sudo docker run -v "$PWD/host_volume_directory":/host_directory alpine touch /host_directory/hello-world 
ls ./host_volume_directory
```
##### Host file as data volume

<details>
    <summary>Click to expand</summary>
    <p> Bind mounts can also be used to mount files, not only directories </p>
</details>

```bash
cat ./host_volume_file
sudo docker run -v "$PWD/host_volume_file":/host_file alpine truncate -s 0 /host_file
cat ./host_volume_file
```

<details>
    <summary>Click to expand</summary>
    <p> We will simulate a development environment by running an apache instance, having access to a local directory, edit the file directly on the host and requesting it via the container </p>
</details>

```bash
sudo docker run -d -p 8001:80 -v "$PWD"/host_directory_httpd:/usr/local/apache2/htdocs/ httpd:2.4
curl localhost:8001
echo "Hello edited content!" > ./host_directory_httpd/index.html
curl localhost:8001
```
##### Data volume containers

<details>
    <summary>Click to expand</summary>
    <p> A frequent method to have a container create a set of volumes and run any subsequent containers with these volumes by referencing the original container with the --volumes-from parameter </p>
    <p> We inspect the data container and it's associated volume </p>
    <p> We run a new container referencing volumes from the data container and check the persistend data </p>
</details>

```bash
sudo docker run -v /workshop-data --name workshop-data-container alpine touch /workshop-data/hello-world
sudo docker inspect workshop-data-container -f '{{.Config.Volumes}}'
sudo docker inspect workshop-data-container -f '{{.Mounts}}'
```
```text
[{volume 54ebd26892c1541b30b63c4789471ffa90019aa71ffcd4545c81948d114570a0 /var/lib/docker/volumes/54ebd26892c1541b30b63c4789471ffa90019aa71ffcd4545c81948d114570a0/_data /workshop-data local  true }]
```
```bash
sudo docker volume inspect 54ebd26892c1541b30b63c4789471ffa90019aa71ffcd4545c81948d114570a0
sudo docker run --volumes-from workshop-data-container alpine ls /workshop-data
```
##### Backup, restore of data volumes

<details>
    <summary>Click to expand</summary>
    <p> A method to export data from a volume is by using the docker cp command. This command can copy data between a host and a container </p>
    <p> We first create a container-volume pair and put some data in the volume </p>
    <p> We will export this data to the host machine, create another container-volume pair and import the data from host to the new volume </p>
</details>

```bash
sudo docker run -v /export-data --name export-data-container alpine touch /export-data/hello-world
sudo docker cp export-data-container:/export-data exported-data
ls exported-data/
sudo docker create -v /import-data --name import-data-container alpine
sudo docker cp exported-data/* import-data-container:/import-data/
sudo docker run --volumes-from import-data-container debian ls /import-data
```
#### Contributing to the ecosystem
##### What is Docker Hub?
hub.docker.com
##### Registering on Docker Hub

##### Command line login

<details>
    <summary>Click to expand</summary>
    <p> Mention: By default, authentication credentials are stored unencrypted in the users's home directory </p>
</details>

```bash
ls ~/.docker/config.json
sudo docker login
```
```text
Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username: alexandrustaetu  
Password: 
Login Succeeded
```
```bash
ls ~/.docker/config.json
```
##### Uploading to Docker Hub

<details>
    <summary>Click to expand</summary>
    <p> The image which we are building must be prefixed with the docker hub username </p>
    <p> We build the image, push it to the docker registry and remove it locally </p>
    <p> We pull it again to make sure it was uploaded to docker hub </p>
</details>

```bash
sudo docker build -t alexandrustaetu/docker-hub-upload ./docker-hub-upload
sudo docker push alexandrustaetu/docker-hub-upload
sudo docker rmi alexandrustaetu/docker-hub-upload
sudo docker pull alexandrustaetu/docker-hub-upload
```
##### Private repositories
![Set private repository](./set_private_repository.png "Set private repository")
```bash
sudo docker rmi alexandrustaetu/docker-hub-upload
sudo docker info
sudo docker logout https://index.docker.io/v1/
sudo docker pull alexandrustaetu/docker-hub-upload
```
```text
Using default tag: latest
Error response from daemon: pull access denied for alexandrustaetu/docker-hub-upload, repository does not exist or may require 'docker login'
```
```bash
sudo docker pull alexandrustaetu/docker-hub-upload
```
```bash
Using default tag: latest
latest: Pulling from alexandrustaetu/docker-hub-upload
Digest: sha256:8c03bb07a531c53ad7d0f6e7041b64d81f99c6e493cb39abba56d956b40eacbc
Status: Downloaded newer image for alexandrustaetu/docker-hub-upload:latest
```
##### Automated builds
