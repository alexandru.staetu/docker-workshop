###### Version
```yaml
version: '3.4'
```

##### Docker services

<details>
    <summary>Click to expand</summary>
    <p> A service is an image for a microservice. This service can be scaled, it is automatically load balanced </p>
    <p> A service can be run on a single docker host or on multiple hosts, where the container are sprea uniformly over the swarm nodes </p>
    <p> When in swarm mode, containers belonging to a service will be connnected to an overlay network, which means that all containers will be placed on the same  subnet </p>
</details>

```bash
docker service create --name redis --replicas=5 redis:3.0.6
```

<details>
    <summary>Click to expand</summary>
    <p> For each container, it's ip address will be added to a dns record </p>
    <p> Containers in a service can be scaled on the fly </p>
    <p> We will run a service that we will scale later and another static service which we will use for investigative purposes </p>
    <p> A new network will be automatically created for  this compose file with local scope ( running one a single node ) of type bridge ( deploying a stack on swarm nodes will create an overlay network which will span over all the nodes ) </p>
    <p> Every container connected to the project's network will be able to resolve dns queries for the service name and also have laod balanced connection to the service ( internal communication ) </p>
</details>

```bash
cd service_discovery
docker-compose up -d
docker-compose ps
docker exec -it servicediscovery_static_1 sh 
> apk update && apk add bind-tools
> dig scaled-service
> exit

docker-compose up --scale scaled-service=10 -d
docker-compose ps
docker exec -it servicediscovery_static_1 sh 
> dig scaled-service
```

##### Using a dockerfile or an image
```bash
cd compose_build_and_image
docker-compose up -d
docker ps --filter "name=composebuildandimage_using"
```

##### Using a host volume

```bash
cd compose_host_volumes
docker-compose up
ls data_volume
```

##### Using a named volume

```bash
cd compose_volumes
docker-compose --f docker-compose-write.yaml up
docker-compose --f docker-compose-read.yaml up
```

##### Service port mapping

```bash
cd compose_ports
docker-compose ps
docker-compose up -d
curl localhost:8080
```

##### Network

```bash
cd compose_network
docker-compose up -d
ping 192.168.111.222 -c 1

docker network ls
docker network inspect composenetwork_compose_network

docker ps
docker inspect composenetwork_static-networked_1
```
##### Variables

```bash
cd compose_variables
docker-compose up -d
docker-compose exec env-variables env
```

##### Resources

```bash
cd compose_resources
docker-compose up -d
ps aux|grep yes
docker-compose down
```

