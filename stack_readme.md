##### Bootup

```bash
docker-machine create swarm-node-1
docker-machine create swarm-node-2
docker-machine create swarm-node-3

docker-machine ls --filter='name=swarm-node'

```

```bash
eval "$(docker-machine env swarm-node-1)"
docker-machine active
```

##### Cluster initialization

```bash
eval "$(docker-machine env swarm-node-1)"
docker-machine ip swarm-node-1
docker swarm init --advertise-addr=$(docker-machine ip swarm-node-1)
```

```text
Swarm initialized: current node (pnif54zx3pm700oufwg9sclf3) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-0qv19j9q9nb8c3lxm9zq42u3ts60o6yrjr3muz5mqvg5plzfk7-77xpz21n1v3tp45eh7w9f3u71 192.168.99.102:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

```bash
docker node ls
```

##### Joining a cluster as a worker node

```bash
eval "$(docker-machine env swarm-node-2)"
docker-machine active
docker swarm join --token SWMTKN-1-0qv19j9q9nb8c3lxm9zq42u3ts60o6yrjr3muz5mqvg5plzfk7-77xpz21n1v3tp45eh7w9f3u71 192.168.99.102:2377

eval "$(docker-machine env swarm-node-3)"
docker-machine active
docker swarm join --token SWMTKN-1-0qv19j9q9nb8c3lxm9zq42u3ts60o6yrjr3muz5mqvg5plzfk7-77xpz21n1v3tp45eh7w9f3u71 192.168.99.102:2377

eval "$(docker-machine env swarm-node-1)"
docker node ls
```

##### Global deploy

```bash
cd stack_global
docker stack deploy -c docker-stack.yaml cadvisor
docker stack ls
docker service ls
docker service ps cadvisor_cadvisor
docker-machine ip swarm-node-1
```

Access in browser the resulting ip on port 8080

##### Placement constraints

```bash
cd stack_placement_constraint
docker stack deploy -c docker-stack.yaml portainer
docker-machine ip swarm-node-1

```

Access in browser the resulting ip on port 9000

##### Service replicas

```bash
cd stack_service_replicas/
docker stack deploy -c docker-stack.yaml replica-stack
docker stack ps replica-stack
docker stack deploy -c docker-stack-scale.yaml replica-stack
```

##### Configs

```bash
cd compose_configs
sudo docker config create hello_external_world.ini hello_external_world.ini
sudo docker config ls
sudo docker config inspect hello_external_world.ini
sudo docker config inspect hello_external_world.ini --format '{{.Spec.Data}}' --pretty
sudo docker stack deploy -c docker-stack.yml stack-config
docker ps
```

```text
sudo docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
6f49f4fae822        alpine:latest       "sleep 3600"        About an hour ago   Up About an hour                        stack-config_service-with-config.1.nvsyxp1fjacxedvjo39ls8gcr
```

```bash
sudo docker exec -it 6f49f4fae822 cat /hello_world.ini
```

```text
hello=world
```

```bash
sudo docker exec -it 6f49f4fae822 cat /hello_external_world.ini
```

```text
hello=external world
```

##### Restart policy

```bash
sudo docker stack deploy -c docker-compose-restart-on-failure.yml restart-on-failure
sudo docker stack ls
sudo docker stack ps restart-on-failure

sudo docker stack deploy -c docker-compose-restart-always.yml restart-always
sudo docker stack ps restart-always
```